import {useContext, useEffect, useState} from 'react';
import {Accordion, Button, Form, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';
import PurchaseRow from './PurchaseRow';
import UserContext from '../UserContext';

export default function Purchases({orderProp}) {
	const {user} = useContext(UserContext);

	const {totalAmount, purchasedOn, userId, orders, status} = orderProp;
	const [order, setOrder] = useState([]);

	function updateStatus(e) {
		e.preventDefault();

		fetch('https://guarded-mesa-27926.herokuapp.com/users/complete', {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				orderId: orderProp._id
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {

				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: "Order status updated to completed succesfully."
				})

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	};

	useEffect(() => {
		setOrder(orders.map(order => {
			return (
				<PurchaseRow key={order._id} productProp={order}/>
			)
		}));
	}, []);

	return(
		<Accordion className="m-3">
			<Accordion.Item>
				<Accordion.Header>
					Order ID: {orderProp._id}
				</Accordion.Header>
				<Accordion.Body>
					<p>Purchase Date: {purchasedOn.substring(0,10)}</p>
					<p>Buyer: {userId}</p>
					<p className="capitalize">Status: {status}</p>
				 <Table>
				 	<thead>
				 		<tr>
				 			<th>#</th>
				 			<th>Product</th>
				 			<th>Quantity</th>
				 			<th>Subtotal</th>
				 		</tr>
				 	</thead>
				 	<tbody>
				 		{order}
				 	</tbody>
				 	<thead>
				 		<tr>
					 		<th colSpan={3}>Total</th>
					 		<th>Php {totalAmount}</th>
				 		</tr>
				 	</thead>
				</Table>
				<Form onSubmit={(e) => updateStatus(e)}>
					{
						(status==="completed" || user.isAdmin === false) ? "" : <Button type="submitBtn">Complete</Button>
					}
				</Form>
				</Accordion.Body>
			</Accordion.Item>
		</Accordion>
	);
};