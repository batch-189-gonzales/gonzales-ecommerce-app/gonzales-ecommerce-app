import {useContext, useState} from 'react';
import {Col, Card, Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({productProp}) {
	const {user, setUser} = useContext(UserContext);
	const {name, description, price, _id, image} = productProp;
	const [isActive, setIsActive] = useState(productProp.isActive);

	function deactivate(e) {
		e.preventDefault();

		fetch(`https://guarded-mesa-27926.herokuapp.com/products/${_id}/archive`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		
		setIsActive(false);
	};

	function activate(e) {
		e.preventDefault();

		fetch(`https://guarded-mesa-27926.herokuapp.com/products/${_id}/unarchive`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		setIsActive(true);
	};

	return(
		<Col className="m-3" md="2">
			<Card className="p-3">
				<Card.Img variant="top" src={image} alt={`Image of ${name}`}/>
				<Card.Body>
					<Card.Title className="text-center">{name}</Card.Title>
					<Card.Subtitle className="mb-0">Description:</Card.Subtitle>
					<Card.Text className="mt-0">{description}</Card.Text>

					<Card.Subtitle className="mb-0">Price:</Card.Subtitle>
					<Card.Text className="mt-0">Php {price}</Card.Text>

					<Button className="me-2 mb-1" size="sm" variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
					{
						(user.isAdmin) ? 
							(isActive) ?
								<Form onSubmit={(e) => deactivate(e)}>
									<Button size="sm" variant="secondary" type="submitBtn">Deactivate</Button>
								</Form>
								:
								<Form onSubmit={(e) => activate(e)}>
									<Button size="sm" variant="primary" type="submitBtn">Activate</Button>
								</Form>
						:
						""
					}
				</Card.Body>
			</Card>
		</Col>
	);
};