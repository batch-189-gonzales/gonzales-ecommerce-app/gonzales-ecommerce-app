import {useState} from 'react';
import {Table} from 'react-bootstrap';

export default function ProductRow({productProp}) {
	const {productId, name, orderQty, subtotal, _id} = productProp;

	return(
		<tr>
			<td>{productId}</td>
			<td>{name}</td>
			<td>{orderQty}</td>
			<td>Php {subtotal}</td>
		</tr>
	);
};