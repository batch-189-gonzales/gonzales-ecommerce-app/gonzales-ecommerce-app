import {useContext} from 'react';
import {Navbar, Nav, Form, Container, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const {user} = useContext(UserContext);

	return (
		<Navbar fixed="top" bg="dark" expand="lg">
			<Container className="text-center">
				<Navbar.Brand as={Link} to="/" className="text-light">Flagship Store</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse>
					<Nav.Link as={Link} to="/products" className="text-secondary">Products</Nav.Link>
					<Form className="d-flex mx-1 px-1">
						<Form.Control type="search" placeholder="Search" className="me-2" aria-label="Search" />
						<Button variant="outline-secondary" type="submitBtn">Search</Button>
					</Form>

					<Container>
						<Nav className="justify-content-end">
								{
								 (user.id !== null) ?
				
									(
									(user.isAdmin) ?

									<>
										<Nav.Link as={Link} to="/orders" className="text-secondary">Orders</Nav.Link>
										<Nav.Link as={Link} to="/logout" className="text-secondary">Logout</Nav.Link>
									</>
									:
									<>
										<Nav.Link as={Link} to="/cart" className="text-secondary">Cart</Nav.Link>
										<Nav.Link as={Link} to="/purchases" className="text-secondary">Purchases</Nav.Link>
										<Nav.Link as={Link} to="/logout" className="text-secondary">Logout</Nav.Link>
									</>
									)
								:
								<>
									<Nav.Link as={Link} to="/login" className="text-secondary">Login</Nav.Link>
									<Nav.Link as={Link} to="/register" className="text-secondary">Register</Nav.Link>
								</>
								}
						</Nav>
					</Container>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
};