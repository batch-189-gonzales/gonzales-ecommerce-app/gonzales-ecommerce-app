import {useState} from 'react';
import {Button, Form, InputGroup, Table} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductRow({productProp}) {
	const {productId, name, orderQty, subtotal, _id} = productProp;
	const navigate = useNavigate();

	const [description, setDescription] = useState("")
	const [image, setImage] = useState("")
	const [price, setPrice] = useState(0)

	function update(e) {
		e.preventDefault();

		navigate(`/products/${productId}`)
	}

	function remove(e) {
		e.preventDefault();

		Swal.fire({
			title: "Remove item from cart?",
			text: "Press Confirm to remove item from cart.",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Confirm"
		})
		.then(result => {
			if(result.isConfirmed) {
				fetch('https://guarded-mesa-27926.herokuapp.com/users/removeFromCart', {
					method: 'POST',
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						productId: productId
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true) {
						window.location.reload();
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						});
					}
				});
			};
		});
	};

	return(
		<tr>
			<td>{productId}</td>
			<td>{name}</td>
			<td>{orderQty}</td>
			<td>
				<Form onSubmit={(e) => update(e)}>
					<Button size="sm" type="submitBtn">update</Button>
				</Form>
			</td>
			<td>Php {subtotal}</td>
			<td>
				<Form onSubmit={(e) => remove(e)}>
					<Button variant="danger" size="sm" type="submitBtn">remove</Button>
				</Form>
			</td>
		</tr>
	);
};