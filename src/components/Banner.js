import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner() {
	return(
		<>
			<Row className="mt-5 pt-3 text-center">
				<Col className="p-5">
					<h1>Flagship Store</h1>
					<p>Your go-to flag store.</p>
					<Link className="btn btn-primary" to="/products">Browse Products</Link>
				</Col>
			</Row>
			<Row className="text-center">
				<Col xs={{span: 4}}>
					<h1>Featured Product</h1>
				</Col>
				<Col  xs={{span: 8}}>
					<h1>Best-seller</h1>
				</Col>
			</Row>
		</>
	);
};