import {Fragment, useEffect, useState} from 'react';
import PurchasesAccordion from '../components/PurchasesAccordion';

export default function Purchases() {
	const [order, setOrder] = useState("");

	useEffect(() => {
		fetch('https://guarded-mesa-27926.herokuapp.com/users/orders', {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrder(data.map(order => {
				return (
					<PurchasesAccordion key={order._id} orderProp={order}/>
				)
			}))
		})
	})
	
	return(
		<Fragment>
			<h1 className="mt-5 pt-3 text-center">Orders</h1>
			{order}
		</Fragment>
	);
};