import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	function clearform() {
		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword1("");
		setPassword2("");
	};

	function registerUser(e) {
		e.preventDefault();

		fetch('https://guarded-mesa-27926.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: "Duplicate Email Found",
					icon: "warning",
					showCancelButton: true,
					confirmButtonText: "Proceed to Login",
					cancelButtonText: "Provide another email"
				})
				.then(result => {
					if(result.isConfirmed) {
						clearform();

						navigate("/login");
					} else if (result.isDismissed) {
						setEmail("");

						Swal.fire('Please provide another email', '', 'info');
					}
				})
			} else {
				fetch('https://guarded-mesa-27926.herokuapp.com/users/register', {
					method: 'POST',
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true) {

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "You may now login."
						})

						clearform();

						navigate("/login");

					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})
	};

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password1 === password2) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2]);

	return(
		(user.id !== null) ? <Navigate to='/products' /> :
		<Form className="mt-5 pt-3" onSubmit={(e) => registerUser(e)}>
			<h1 className="text-center">Register</h1>
			<Form.Group className="mb-3" controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>
			
			<Form.Group className="mb-3" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
			</Form.Group>
			{
				isActive ?
					<Button variant="primary" type="submitBtn">
					Submit
					</Button>
					:
					<Button variant="secondary" type="submitBtn" disabled>
					Submit
					</Button>
			}
		</Form>
    );
};