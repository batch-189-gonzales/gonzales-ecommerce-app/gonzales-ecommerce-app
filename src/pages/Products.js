import {Fragment, useEffect, useContext, useState} from 'react';
import {Button, Col, Form, Modal, Row} from 'react-bootstrap';
import {Button as FloatingButton, Container, lightColors} from 'react-floating-action-button';
import Swal from 'sweetalert2';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products() {
	const {user, setUser} = useContext(UserContext);
	const [product, setProduct] = useState([])

	const [show, setShow] = useState(false);

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imageLink, setImageLink] = useState(undefined);

	const [isActive, setIsActive] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	function addProduct(e) {
		e.preventDefault();

		fetch('https://guarded-mesa-27926.herokuapp.com/products', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: description,
				price: price,
				image: imageLink
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {

				Swal.fire({
					title: "Add Successful",
					icon: "success",
					text: "Product added successfully."
				})

				setProductName("");
				setDescription("");
				setPrice(0);
				setImageLink("");

				setShow(false);
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	};

	useEffect(() => {
		if(user.isAdmin===true) {
			fetch('https://guarded-mesa-27926.herokuapp.com/products/admin', {
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setProduct(data.map(product => {
					return (
						<ProductCard key={product._id} productProp={product}/>
					)
				}))
			})
		} else {
			fetch('https://guarded-mesa-27926.herokuapp.com/products')
			.then(res => res.json())
			.then(data => {
				setProduct(data.map(product => {
					return (
						<ProductCard key={product._id} productProp={product}/>
					)
				}))
			})
		}
	}, [user.isAdmin, show])

	useEffect(() => {
		if(productName !== '' && description !== '' && price !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [productName, description, price]);
	
	return(
		<Fragment>
			<h1 className="mt-5 pt-3 text-center">Products</h1>
			<Row className="justify-content-center">
				{product}
			</Row>

		<Modal show={show} onHide={handleClose} animation={false}>
        	<Modal.Header closeButton>
        		<Modal.Title>Add Product</Modal.Title>
        	</Modal.Header>
        	<Modal.Body>
        		<Form className="mt-3" onSubmit={(e) => addProduct(e)}>
					<Form.Group className="mb-3" controlId="productName">
						<Form.Label>Name</Form.Label>
						<Form.Control type="text" placeholder="Enter product name" value={productName} onChange={e => setProductName(e.target.value)} required/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="description">
						<Form.Label>Description</Form.Label>
						<Form.Control type="textarea" placeholder="Enter description" value={description} onChange={e => setDescription(e.target.value)} required/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="price">
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" placeholder="Enter price" value={price} onChange={e => setPrice(e.target.value)} required/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="imageLink">
						<Form.Label>Image Link</Form.Label>
						<Form.Control type="text" placeholder="Enter image link" value={imageLink} onChange={e => setImageLink(e.target.value)} />
					</Form.Group>

					{
						isActive ?
							<Button variant="primary" type="submitBtn">
							Add Product
							</Button>
							:
							<Button variant="secondary" type="submitBtn" disabled>
							Add Product
							</Button>
					}

					<Button className="m-1" variant="secondary" onClick={handleClose}>Close</Button>
				</Form>
        	</Modal.Body>
		</Modal>

			{
				(user.isAdmin) ?
				<Container className="justify-content-end">
					<FloatingButton
						tooltip="Add Product"
		             	styles={{backgroundColor: '#0D6EFD', color: "#FFFFFF"}}
		                onClick={handleShow}
					>Add</FloatingButton>
				 </Container>
				 :
			 ""
			}
		</Fragment>
	);
};