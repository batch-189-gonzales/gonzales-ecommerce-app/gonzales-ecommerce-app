import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Lougout() {
	//localStorage.clear();
	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	useEffect(() => {
		setUser({id: null});
	});

	return(
		<Navigate to="/" />
	);
};