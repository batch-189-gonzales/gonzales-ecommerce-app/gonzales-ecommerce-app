import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
	const {user, setUser} = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function loginUser(e) {
		e.preventDefault();

		fetch('https://guarded-mesa-27926.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome!"
				})

				setEmail("");
				setPassword("");

			} else if(data.msg === "user") {
				Swal.fire({
					title: "User not found!",
					icon: "error",
					showCancelButton: true,
					confirmButtonText: "Register",
					cancelButtonText: "Try another email"
				})
				.then(result => {
					if(result.isConfirmed) {
						setEmail("");
						setPassword("");

						navigate("/register");
					} else if (result.isDismissed) {
						setEmail("");
					}
				})
			} else if(data.msg === "password") {
				Swal.fire({
					title: "Incorrect password!",
					icon: "error"
				})

				setPassword("");
			} else {
				Swal.fire({
					title: "Opps!",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		})
	}

	const retrieveUserDetails = (token) => {
		fetch('https://guarded-mesa-27926.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	return(
		(user.id !== null) ? <Navigate to='/products' /> :

		<Form className="mt-5 pt-3" onSubmit={(e) => loginUser(e)}>
			<h1 className="text-center">Login</h1>
			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
			</Form.Group>
			
			{
				isActive ?
					<Button variant="primary" type="submitBtn">
					Login
					</Button>
					:
					<Button variant="secondary" type="submitBtn" disabled>
					Login
					</Button>
			}
		</Form>
    );
};