import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Form, InputGroup, Modal, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function	ProductView() {
	const {user} = useContext(UserContext);

	const navigate = useNavigate()

	const {productId} = useParams()
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [image, setImage] = useState("")

	const [qty, setQty] = useState(1)

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [isActive, setIsActive] = useState(false);

	const addToCart = (productId) => {
		fetch('https://guarded-mesa-27926.herokuapp.com/users/checkout', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				orderQty: qty
			})
		})
	}

	function updateProduct(e) {
		e.preventDefault();

		fetch(`https://guarded-mesa-27926.herokuapp.com/products/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {

				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: "Product updated successfully."
				})

				setName("");
				setDescription("");
				setPrice(0);
				setImage("");

				setShow(false);
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {
		fetch(`https://guarded-mesa-27926.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImage(data.image)
		})

	}, [productId, show]);

	return(
		<Container className="mt-5 pt-5">
			<Row className="justify-content-center">
				<Col md={{span:4}}>
					<img className="img-fluid" src={image} alt={`Image of ${name}`}/>
				</Col>
				<Col md={{span:4, offset: 2}}>
					<Card className="text-center p-3">
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle className="mb-0">Description:</Card.Subtitle>
							<Card.Text className="mt-0">{description}</Card.Text>

							<Card.Subtitle className="mb-0">Price:</Card.Subtitle>
							<Card.Text className="mt-0">Php {price}</Card.Text>

							{
								user.id !== null ?

									user.isAdmin ?

									<Button onClick={handleShow}>Edit</Button>

									:
								
									<>
										<Col xs={{span: 4, offset: 4}}>
											<InputGroup className="mb-1">
												<Button variant="primary" onClick={e => {
													if(qty > 1) {
														setQty(qty-1)
													}
												}}>-</Button>
												<Form.Control type="number" placeholder={qty} value={qty} onChange={e => setQty(e.target.value)} required/>
												<Button variant="primary" onClick={e => setQty(qty+1)}>+</Button>
											</InputGroup>
										</Col>

										<Button variant="primary" onClick={() => {
											addToCart(productId)

											setQty(1)
											navigate("/cart");
										}}>Add to Cart</Button>
									</>
								:
								<Link className="btn btn-danger" to="/login">Login to Buy</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>

			<Modal show={show} onHide={handleClose} animation={false}>
	        	<Modal.Header closeButton>
	        		<Modal.Title>Update Product</Modal.Title>
	        	</Modal.Header>
	        	<Modal.Body>
	        		<Form className="mt-3" onSubmit={(e) => updateProduct(e)}>
						<Form.Group className="mb-3" controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" placeholder="Enter product name" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="description">
							<Form.Label>Description</Form.Label>
							<Form.Control type="textarea" placeholder="Enter description" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="price">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" placeholder="Enter price" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="imageLink">
							<Form.Label>Image Link</Form.Label>
							<Form.Control type="text" placeholder="Enter image link" value={image} onChange={e => setImage(e.target.value)} />
						</Form.Group>

						<Button variant="primary" type="submitBtn">Update Product</Button>

						<Button className="m-1" variant="secondary" onClick={handleClose}>Close</Button>
					</Form>
	        	</Modal.Body>
			</Modal>
		</Container>
	)
}