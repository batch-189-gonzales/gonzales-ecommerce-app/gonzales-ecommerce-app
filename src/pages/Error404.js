import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error() {

    return (

        <Row>
            <Col className="p-5">
                <h1>Error 404: Page Not Found</h1>
                <p>The page you are trying to access doesn't exist.</p>
                <Button as={ Link } to="/" variant="primary" >Go back to home</Button>
            </Col>
        </Row>

    );

};