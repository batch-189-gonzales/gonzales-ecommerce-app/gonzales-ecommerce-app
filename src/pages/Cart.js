import {Fragment, useContext, useEffect, useState} from 'react';
import {Button, Form, Table} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import ProductRow from '../components/ProductRow';
import UserContext from '../UserContext';

export default function Products() {
	const navigate = useNavigate();

	const [product, setProduct] = useState([]);

	const [total, setTotal] = useState(0);
	const [cartId, setCartId] = useState("");

	function checkout(e) {
		e.preventDefault();

		fetch('https://guarded-mesa-27926.herokuapp.com/users/checkout', {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			navigate("/purchases");
		})
	};

	useEffect(() => {
		fetch('https://guarded-mesa-27926.herokuapp.com/users/myCart', {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setTotal(data[0].totalAmount)

			setProduct(data[0].orders.map(product => {
				return (
					//console.log(product.productId)
					<ProductRow key={product.productId} productProp={product}/>
				)
			}))
		})
	}, [total, product])
	
	return(
		(localStorage.getItem('token') === null) ?

		<Navigate to='/login' />

		:

		<Fragment>
		<h1 className="mt-5 pt-3 text-center">My Cart</h1>
		    <Table striped bordered hover>
		      <thead>
		        <tr>
		        	<th>#</th>
		        	<th>Product</th>
		        	<th>Quantity</th>
		        	<th>Update</th>
		        	<th>Subtotal</th>
		        </tr>
		      </thead>
		      <tbody>
		      	{product}
		      </tbody>
		      <thead>
		      	<tr>
		      		<th colSpan={4}>Total</th>
		      		<th>Php {total}</th>
		      	</tr>
		      </thead>
		    </Table>

		    <Form onSubmit={(e) => checkout(e)}>
		    	{(total !== 0) ?
			    <Button type="submitBtn">Checkout</Button> : ""}
			</Form>
   		</Fragment>
	);
};