import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Cart from './pages/Cart';
import Error404 from './pages/Error404';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Purchases from './pages/Purchases';
import Orders from './pages/Orders';
import Register from  './pages/Registration';
import './App.css';
import {UserProvider} from'./UserContext';

function App() {
    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch('https://guarded-mesa-27926.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined') {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/cart" element={<Cart/>} />
          <Route path="/products" element={<Products/>} />
          <Route path="/products/:productId" element={<ProductView/>} />
          <Route path="/purchases" element={<Purchases/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="/orders" element={<Orders/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="*" element={<Error404/>} />
        </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
